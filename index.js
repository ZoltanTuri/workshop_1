const argv = require("yargs").argv;

const verbose = argv && argv.verbose;
const startBet = argv && argv.bet;
var balance = argv && argv.balance;
var stopWin = argv && argv.stopWin;

if (!startBet || !balance || startBet <= 0 || balance <= 0)
{
  console.log(" Invalid Bet or Balance! ");
  return;
}
else
{
  var num1 = parseFloat(startBet);
  var num2 = parseFloat(balance);
  if (!num1 || !num2)
  {
    console.log("Invalid Bet or Balance!");
    return;
  }
}
if (stopWin)
{
  var num = parseFloat(stopWin);
  if (!num)
  {
    console.log("Wrong stopWin param.     stopWin = 20%");
    stopWin = 20;
  }
  else
  {
    stopWin = num;
  }
}
else
{
  stopWin = 20;
}

var bet = startBet;
var round = 0;
var profit = balance * (1 + stopWin / 100);
var label = "";
while (true)
{
  round++;
  if (bet > balance)
  {
    bet = balance;
  }
  balance -= bet;
  var isWon = Math.random() < (18 / 38); // isRed
  if (isWon)
  {
    balance += 2 * bet;
    label = `'Won ${bet}`;
    bet = startBet;
  }
  else
  {
    label = `'Lost ${bet}`;
    bet *= 2;

  }
  if (verbose)
  {
    console.log(`${label}, balance: ${balance}'`);
  }
  if (balance <= 0)
  {
    console.log(`\n  "Bankruptcy!"   \n  Lost in ${round} rounds`);
    return;
  }
  if (balance >= profit)
  {
    console.log("\n Won in " + round + " rounds.  Balance: " + balance);
    return;
  }
}




